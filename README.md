# Data Analysis in R

### An Analysis of Suicide Rates and the Effects of Socioeconomic Factors

This project was part of an assessment piece, our assignment was to gather data, formulate a research question and obtain meaningful interpretation through statistical analysis. To code used for this project can be viewed in "Data Analysis Code.Rmd", the presentation slides in the powerpoint and data collected in master.csv. The following is the speech I delivered with the powerpoint presentation.

#### 1
The World Health Organisation reported figures show that on average almost 800,000 people die each year from suicide. And currently in Australia, suicide is the leading cause of death in people aged 15 to 44. The causes of suicide are generally attributed to mental illnesses as 87% of recorded suicide cases the victim has had a history of psychiatric disorders. And the effects of these mental health problems are pervasive across all countries and regions of the world. 

#### 2
Suicide and suicide attempts have a significant impact on families, friends, colleagues and societies. Suicides are preventable and much can be done to prevent suicide at individual, community and national levels. In this analysis I sought to understand the socio-economic effects on suicide rates and identify any significant factors or trends in rates globally and regionally. 

#### 3
Before my analysis the first step I took was to inspect the data. I found missing data for the variable HDI and the year 2016 so removed both. Also, after inspecting the variable generation in a table with age I found an overlap so used age groups for the purposes of this analysis. I also transformed the categorical data and ordered the values by ascending order as well as adding a column for continent.

#### 4
Starting with some exploratory data analysis, I graphed the global suicide rate per 100 thousand people over the years and added the mean line. As you can see the global trend is decreasing from the peak of over 15 suicides per 100 thousand in 1995 below the global mean of 13 to around 11.5 per 100 thousand in 2015. This is a decrease of approximately 25% over the last 20 years. 

#### 5
Next, I graphed the trend of each continent over time. I found historically Europe has the highest rate of suicide however has been trending downwards and now equals the rate of the America and Oceanic regions. I noticed there’s also a slight upwards trend in Americas and Oceania otherwise all are decreasing. Another interesting note is Africa around 1995 to 1996 where rate drops to around only 1-3 per 100k. This is most likely from misrepresentation as there is only data for 3 of 54 countries in Africa. 

#### 6
Here I plot the proportion of suicides per 100 thousand that were attributed to male’s vs those attributed to females. I found on average the proportion of males to females was 3.5 times more. Research shows this is due to more lethal methods used by males however suicide attempts are found to be more common in females. Gender differences play a significant role in suicide.

#### 7
Another interesting factor of suicide rates is the effect of age. Globally the rate of suicide increases with age with on average a suicide rate of around 24 per 100 thousand for people ages 75. 

#### 8
Finally, I set out to answer how does a country’s GDP affect suicide rates? First, I asked as a country gets richer, does it’s suicide rate decrease? However, it depends on the country. Generally, GDP per capita increases as time goes. There is a very strong positive linear relationship between GDP and year. So this question is like, does a countries suicide rate decrease over time? The trend globally is obviously already going down as time goes on however it depends on the country as trends are different for each. So instead the question should be do richer countries have a higher rate of suicide? To answer this, I calculated the mean GDP per capita for each country over all years then measured how this relates to a countries suicide rate. Plotting this effect, we can see a few outliers that may have a significant impact on the regression models fit. 

#### 9
I measured the Cook’s Distance to identify any significant outliers and excluded these in the final model. Cook’s Distance identified 5 countries as having the most significant effect on the models fit.

#### 10
Using linear regression, I measured to effect of the wealth of a country on its suicide rate and found a weak but significant positive linear relationship. Richer countries are associated with higher rates of suicide.

#### 11
I noted the following in the regression diagnostics, p-value was lower than point zero 5, so reject there is no effect of GDP. r-squared = 0.0493, GDP per capita explains ~5% of variance in suicide rate globally. And the regression equation is, Suicide Rate = 8.378 + 0.1054 x GDP per Capita in thousands. Finally, I found a GDP per capita increase of approximately $9,588 is associated with 1 additional suicide per 100 thousand people per year.

#### 12
Concluding, these are some of the key insights I found in my analysis. Richer countries have higher suicide rates, incidence of suicide increases with age, globally suicide rates are decreasing however trending upwards in Oceanic and Americas regions.

#### Reference Slide

