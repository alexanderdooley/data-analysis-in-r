---
title: "CAB220_A3"
author: "Alex Dooley"
date: "10/13/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r echo=FALSE, comment=NA, message=FALSE, warning=FALSE}
library(ggalt)
library(gridExtra)
library(tidyverse)
library(broom)
library(rworldmap)
library(countrycode)
library(RColorBrewer)

data <- read_csv("master.csv") 
summary(data) # over 19k missing for HDI
table(data$age, data$generation) # overlap between age grp and generations

data <- data %>% 
  select(-c(`HDI for year`, `suicides/100k pop`, `generation`)) %>%
  rename(gdp_for_year = `gdp_for_year ($)`, 
         gdp_per_capita = `gdp_per_capita ($)`, 
         country_year = `country-year`) %>%
  as.data.frame()

 # remove year 2016
data <- data %>%
  filter(year != 2016) %>% 
  select(-country_year)

# capitalise gender
data$sex <- ifelse(data$sex == "male", "Male", "Female")

# remove years suffix from age group 
data$age <- gsub(" years", "", data$age)

# assign country code to string values and group by continent
data$continent <- countrycode(sourcevar = data[, "country"],
                              origin = "country.name",
                              destination = "continent")

# categorical variables
catData <- c('sex', 'continent', 'country')
data[catData] <- lapply(data[catData], 
                        function(i)
                          {
                            factor(i)
                          })


# order factors of categorical data
data$age <- factor(data$age, 
                   ordered = T, 
                   levels = c("5-14",
                              "15-24", 
                              "25-34", 
                              "35-54", 
                              "55-74", 
                              "75+"))
# add altered data to df
data <- as_tibble(data)


# calculate global average suicide per 100k
globalMeanSuicide <- (sum(as.numeric(data$suicides_no)) / sum(as.numeric(data$population))) * 100000

print(globalMeanSuicide)
```

# Macro-Level Analysis

```{r echo=FALSE, comment=NA, message=FALSE, warning=FALSE}
# graph suicide data globally
data %>%
  group_by(year) %>%
  summarize(population = sum(population), 
            suicides = sum(suicides_no), 
            suicides_per_100k = (suicides / population) * 100000) %>%
  ggplot(aes(x = year, 
             y = suicides_per_100k)) + 
    theme_light() + 
    geom_line(col = "#e3a6cb", size = 1) + 
    geom_hline(yintercept = globalMeanSuicide, 
               linetype = 3, 
               color = "black", 
               size = 0.75) +
    labs(title = "Suicides Per 100k People Globally",
       x = "Year", 
       y = "Suicides/100k") + 
  scale_x_continuous(breaks = seq(1984, 2016, 2))

print(data$suicides_per_100k)
```

# Graph by Continent
```{r echo=FALSE, comment=NA, message=FALSE, warning=FALSE}
continent <- data %>%
  group_by(continent) %>%
  summarize(suicide_per_100k = (sum(as.numeric(suicides_no)) / sum(as.numeric(population))) * 100000) %>%
  arrange(suicide_per_100k)

continent$continent <- factor(continent$continent, 
                              ordered = TRUE, 
                              levels = continent$continent)

continentSuicides <- data %>%
  group_by(year, continent) %>%
  summarize(suicide_per_100k = (sum(as.numeric(suicides_no)) / sum(as.numeric(population))) * 100000)

continentSuicides$continent <- factor(continentSuicides$continent, ordered = TRUE, levels = continent$continent)

continentSuicidesGraph <- ggplot(continentSuicides, 
                              aes(x = year, 
                                  y = suicide_per_100k, 
                                  col = continent)) 

continentSuicidesGraph +
  theme_light() + 
  scale_color_brewer(type = "qual", palette = "Set2") + 
  geom_line(size = 1) +
  geom_point() +
  labs(title = "Yearly Suicides Per 100k by Continent",
       x = "Year",
       y = "Suicides/100k",
       color = "Continent") +
  scale_x_continuous(breaks = seq(1985, 2015, 2), minor_breaks = FALSE)

```

# By Gender
```{r echo=FALSE, comment=NA, message=FALSE, warning=FALSE}
genderSuicidesOverTime <- data %>%
  group_by(year, sex) %>%
  summarize(suicide_per_100k = (sum(as.numeric(suicides_no)) / sum(as.numeric(population))) * 100000)
  
genderSuicidesGraph <- ggplot(genderSuicidesOverTime, 
                              aes(x = year, 
                                  y = suicide_per_100k, 
                                  col = factor(sex)))

genderSuicidesGraph +
  theme_light() + 
  scale_color_brewer(type = "qual", palette = "Set2") + 
  geom_line(size = 1) +
  geom_point() +
  labs(title = "Suicides Per 100k by Gender",
       x = "Year",
       y = "Suicides/100k",
       color = "Sex") +
  scale_y_continuous(limits = c(0, 30), expand = c(0, 0), breaks = seq(0, 30, 5)) +
  scale_x_continuous(breaks = seq(1985, 2015, 2), minor_breaks = FALSE)

```

# By Age

```{r echo=FALSE, comment=NA, message=FALSE, warning=FALSE}
ageSuicidesTotal <- data %>%
  group_by(age) %>%
  summarize(suicide_per_100k = (sum(as.numeric(suicides_no)) / sum(as.numeric(population))) * 100000)
  
globalSuicidesByAge <- ggplot(ageSuicidesTotal, 
                              aes(x = age, 
                                  y = suicide_per_100k, 
                                  fill = age)) 

globalSuicidesByAge +
  theme_light() + 
  theme(legend.position = "none") +
  scale_fill_brewer(type = "qual", palette = "Set2") + 
  geom_bar(stat = "identity") +
  labs(title = "Suicides Per 100k by Age",
       x = "Age",
       y = "Suicides/100k") +
  scale_y_continuous(breaks = seq(0, 26, 1), minor_breaks = FALSE)


ageSuicidesOverTime <- data %>%
  group_by(year, age) %>%
  summarize(suicide_per_100k = (sum(as.numeric(suicides_no)) / sum(as.numeric(population))) * 100000)
  
  
globalSuicidesByAgeTime <- ggplot(ageSuicidesOverTime, 
                                  aes(x = year, 
                                      y = suicide_per_100k, 
                                      col = age)) 

globalSuicidesByAgeTime + 
  theme_light() + 
  scale_color_brewer(type = "qual", palette = "Set2") + 
  geom_line(size = 1) + 
  geom_point() + 
  labs(title = "Suicides Per 100k by Age Over Time", 
       x = "Year", 
       y = "Suicides/100k", 
       color = "Age") + 
  scale_x_continuous(breaks = seq(1985, 2015, 2), minor_breaks = FALSE)

```

Including all  
```{r echo=FALSE, comment=NA, message=FALSE, warning=FALSE}
meanSuicidesGDPByCountry <- data %>%
  group_by(country, continent) %>%
  summarize(suicide_per_100k = (sum(as.numeric(suicides_no)) / sum(as.numeric(population))) * 100000,
            gdp_per_capita = mean(gdp_per_capita))

correlationGDPSuicides <- ggplot(meanSuicidesGDPByCountry, 
                aes(x = gdp_per_capita, 
                    y = suicide_per_100k, 
                    col = continent)) 

correlationGDPSuicides +
  theme_light() + 
  scale_color_brewer(type = "qual", palette = "Set1") +
  geom_point() +
  scale_x_continuous(breaks = seq(0, 80000, 10000), minor_breaks = TRUE) + 
  labs(title = "GDP vs Suicides Per 100K",
       x = "$GDP Per Capita",
       y = "Suicides per 100k",
       col = "Continent")


```


```{r echo=FALSE, comment=NA, message=FALSE, warning=FALSE}
lmMod <- lm(suicide_per_100k ~ gdp_per_capita, data = meanSuicidesGDPByCountry)

cooksd <- cooks.distance(lmMod)
plot(cooksd, main="Influential Observations by Cooks Distance")
abline(h = 4*mean(cooksd, na.rm=T), col="red")

n <- nrow(meanSuicidesGDPByCountry)
influential <- as.numeric(names(cooksd)[(cooksd > (4/n))])

dirtyMeanSuicidesGDPByCountry <- meanSuicidesGDPByCountry[influential, ]
print(dirtyMeanSuicidesGDPByCountry)

cleanMeanSuicidesGDPByCountry <- meanSuicidesGDPByCountry[-influential, ]

correlationGDPSuicides2 <- ggplot(cleanMeanSuicidesGDPByCountry, 
                aes(x = gdp_per_capita, 
                    y = suicide_per_100k, 
                    col = continent)) 

correlationGDPSuicides2 +
  theme_light() + 
  scale_color_brewer(type = "qual", palette = "Set1") + 
  geom_smooth(method = "lm", se = FALSE, aes(group = 1)) + 
  geom_point() +
  scale_x_continuous(breaks = seq(0, 80000, 10000), minor_breaks = TRUE) + 
  labs(title = "GDP vs Suicides Per 100K",
       x = "$GDP Per Capita",
       y = "Suicides per 100k",
       col = "Continent")

lmMod2 <- lm(suicide_per_100k ~ gdp_per_capita, data = cleanMeanSuicidesGDPByCountry)
summary(lmMod2)
# model2 <- lm(suicide_per_100k ~ gdp_per_capita, data = gdp_suicide_no_outliers)
# 
# summary(model2)
```
p-value < 0.05, reject there is no effect.
r-squared = 0.0493, GDP per capita explains ~5% of variance in suicide rate globally
weak but significant positive linear relationship.